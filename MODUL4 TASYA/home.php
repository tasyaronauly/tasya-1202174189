<html>
<head>
    <title>Home</title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="home.css">
    </head>
<body>
    	
    <nav class="navbar navbar-default">
        <div class="container-fluid">
    <div class="navbar-header">
      <ul class="nav navbar-nav navbar-right">
       <li><a href="#" style="position: absolute;margin:-10 1360"data-toggle="modal" data-target="#register"><span class="glyphicon glyphicon-user"></span> Daftar</a></li>
       <li><a href="#"style="position: absolute;margin:-10 1300"data-toggle="modal" data-target="#login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
  </ul>
  
   </div>
</div>
<img class ="ead"src="EAD.PNG" >
	<div class="intro">
	<p style="font-size: 80px; color: white;">Hello Coders</p>
	<p style="color: white;">Welcome to our store, please take a look for the products you might buy</p>
	</div>
	<div class="product1">
		<button type="button" class="btn btn-warning">Buy</button>
		<img src="web.jpg"width="150" height="150" style="position: absolute; top: 50; right:40">
		<div class="tipe1" style="text-align: center;"><b>Learning Basic Web Programming</b>
		<div style="text-align: left">Rp. 210.000,- </div>
		<div style="font-size: 15; text-align: left"> Want to be able to make a website? Learn basic components such as HTML, CSS and JavaScript in this class curiculum</div>
		</div>
	</div>
	<div class="product2">
		<button type="button" class="btn btn-warning">Buy</button>
		<img src="java.png" width="150" height="150" style="position: absolute; top: 50; right:40">
		<div class="tipe2" style="text-align: center"><b>Starting Programming in Java</b>
		<div style="text-align: left">Rp. 150.000,- </div>
		<div style="font-size: 15; text-align: left">Learn Java Languange for you who want ti learrn the most popular Object-Oriented Proramming (PBO) concept for developing applications</div>
		</div>
	</div>
	<div class="product3">
		<button type="button" class="btn btn-warning">Buy</button>
		<img src="phyton.png"width="150" height="150" style="position: absolute; top: 50; right:40">
		<div class="tipe3">
		<div style="text-align: center"><b>Starting Programming in Phyton</b>
		<div style="text-align: left">Rp. 200.000,- </div>
		<div style="font-size: 15; text-align: left">Learn Phyton-Fundamental various current industry trends: Data Science, Machine Learning, Infrastructure-management.</div>
		</div> 	
		</div>
	</div>
	<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="login">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="modal-title" id="login">Login</div>
                    </div>
                  	<div class="modal-body">
                    <!-- form login -->
                    <form method="post" action="cek_login.php" class="form-horizontal">
                	  <div class="form-group">
                  	<label for="nama" class="col-sm-2 control-label">Username</label>
                  	<div class="col-sm-10">
                  	<input type="text" class="form-control" name="username" requered ></div>
            		    </div>
            		    <div class="form-group">
                  	<label for="nama" class="col-sm-2 control-label">Password</label>
                  	<div class="col-sm-10">
                  	<input type="password" class="form-control" name="password" requered ></div>
                  	</div>
                  	<div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" name="log"class="btn btn-primary">LOGIN</button>
                  	</div>
                    </form>
                </div>
              </div>
            </div>
            </div>
</div>
	<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="register">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="modal-title" id="register">Registration</div>
                    </div>
                  	<div class="modal-body">
                    <!-- form registrasi -->
                    <form method="POST" action="daftar.php" class="form-horizontal">
                	  <div class="form-group">
                  	<label for="nama" class="col-sm-2 control-label">Email</label>
                  	<div class="col-sm-10">
                  	<input type="email" class="form-control" name="email" required ></div>
            		    </div>
            		    <div class="form-group">
                  	<label for="nama" class="col-sm-2 control-label">Username</label>
                  	<div class="col-sm-10">
                  	<input type="text" class="form-control" name="username" required ></div>
            	     	</div>
            		    <div class="form-group">
                  	<label for="nama" class="col-sm-2 control-label">Mobile Number</label>
                  	<div class="col-sm-10">
                  	<input type="number" class="form-control" name="nohp" required ></div>
            		    </div>
            		    <div class="form-group">
                  	<label for="nama" class="col-sm-2 control-label">Password</label>
                  	<div class="col-sm-10">
                  	<input type="password" class="form-control" name="password" required ></div>
                  	</div>
                  	<div class="form-group">
                  	<label for="nama" class="col-sm-2 control-label">Confirm Password</label>
                  	<div class="col-sm-10">
                  	<input type="password" class="form-control" required=""></div>
                  	</div>
                  	<div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" name="regis"class="btn btn-primary">register</button>
                  	</div>
                    </form>
                </div>
              </div>
            </div>
          
</div>
		
</body>
</html>
